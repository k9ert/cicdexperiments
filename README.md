# cicdexperiments

For now, this repo should demonstrate the use of the build.sh-script in the utils-folder.

As a simple usecase, we assume that the successfull build of this project depends on a more or less complex [dockerimage](./docker/somedependency) which will spit out this readme-file in the build-job of the gitlab-ci.yml.



## Prerequisites

In order for the build.sh to work, you need to authorize the build-script to push to the code-repository.
To do that:
* See the lines 0-50 in .gitlab-ci.yaml and copy it to your project. 
* Create a private key like this:
```
ssh-keygen -t rsa -C gitlab-cicd-variables
```
* Set the variable GITLAB_KNOWN_HOSTS in gitlab-project/settings/CICD/Variables/ :
```

```
* Also this the var SSH_PRIVATE_KEY: the private part of the key you created above
* Take the public key and authorize it at gitlab-project/settings/Repository/DeployKeys (don't forget to tick "Write access allowed")

