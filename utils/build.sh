#!/bin/bash
set -e

echo ""
echo "Build.sh - lazybuild images"
echo "==========================="
echo ""

# Use it e.g. like this:
# utils/build.sh --image 'registry.gitlab.com/cryptoadvance/specter-cloud/pythonbase' 
#                --imageref .gitlab-ci.yml 
#                --imageref Dockerfile 
#                --inputfile requirements.txt 
#                --builddir docker/pythonbase"
# 
# This would build a Dockerimage in builddir, tag and push it 
# only if the references in gitlab.ci.yml or Dockerfile are older
# then the requirements.txt 
# AFter that it will update the references in the imagerefs, git push the change
# and finally return non-zero to let the build fail
# Another one is kicked off by the above commit which will take the new
# build into account. 


parse_yaml() {
   local prefix=$2
   local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
   sed -ne "s|^\($s\)\($w\)$s:$s\"\(.*\)\"$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |
   awk -F$fs '{
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
         printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
      }
   }'
}
# We can have more then one imageref
IMAGEREFS=()
INPUTFILES=()

while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -i|--image)
    IMAGE="$2"
    shift # past argument
    shift # past value
    ;;
    -r|--imageref)
    IMAGEREFS+=" $2"
    shift # past argument
    shift # past value
    ;;
    -f|--inputfile)
    INPUTFILES+=" $2"
    shift # past argument
    shift # past value
    ;;
    -b|--builddir)
    BUILDDIR="$2"
    shift # past argument
    shift # past value
    ;;
    --dry-run)
    DRYRUN=YES
    shift # past argument
    ;;
    --debug)
    set -x
    shift # past argument
    ;;
    *)    # unknown option
    POSITIONAL="$1" # save it in an array for later
    shift # past argument
    ;;
esac
done

# Do we need to build?

# We need to build if 
# any of the INPUT_FILES is newer
# then the oldest IMAGEREF to that image

# So we need to find the oldest INPUTFILE 
# and the newest IMAGEREF


# Let's compare timestamps:
# 1. timestamp of used docker-image
# Figure out the TS_IMAGE dependent on the type of file referencing it
#TS_IMAGE=$(date +%Y%m%d%H%M%S)
TS_IMAGE=20990000000000
echo "    --> Examining the oldest IMAGEREF"
for IMAGEREF in $IMAGEREFS ; do
  # We'll need the oldest reference to this image
  if [[ $IMAGEREF == ".gitlab-ci.yml" ]]; then
    echo $(parse_yaml $IMAGEREF "test")
    TS_IMAGEREF=$(parse_yaml $IMAGEREF "test" | grep $IMAGE | cut -d= -f2 | sed -e 's/"//g' | cut -d: -f2 | head -1)
    echo "# $IMAGEREF is a .Gitlab-ci.yml file with $TS_IMAGEREF"
  elif [[ $IMAGEREF == *"Dockerfile" ]]; then
    TS_IMAGEREF=$(cat $IMAGEREF | head -1 | cut -d: -f2)
    echo "# $IMAGEREF is a Dockerfile with $TS_IMAGEREF"
  else
    echo "# ERROR: unknown Imageref-file: $IMAGEREF"
    exit 4
  fi
  if [ $TS_IMAGE -gt $TS_IMAGEREF ]; then
    echo "# $IMAGEREF is older!"
    TS_IMAGE=$TS_IMAGEREF
  fi
done

echo "   --> The oldest IMAGEREF results in TS_IMAGE=$TS_IMAGE"

# 2. timestamp of INPUTFILE
TS_INPUTFILE=19900000000000
echo "   --> Examining the newest INPUTFILE"
for INPUTFILE in $INPUTFILES ; do
  # We'll need the newest INPUTFILE for this image
  MY_TS=$(git log --pretty=format:%cd -n 1 --date=iso -- "$INPUTFILE" | sed -e "s/[- :]//g" -e "s/+.*//")
  echo "# Examining $INPUTFILE has timestamp $MY_TS"
  if [ "$TS_INPUTFILE" -lt "$MY_TS" ]; then
    echo "# $INPUTFILE is newer!"
    TS_INPUTFILE=$MY_TS
  fi
done
echo "   --> The newest INPUTFILE results in TS_INPUTFILE=$TS_INPUTFILE"
echo "# $INPUTFILE is $TS_INPUTFILE"
echo ""
echo "#      TSIMAGE=$TS_IMAGE"
echo "# TS_INPUTFILE=$TS_INPUTFILE"
echo ""

if [ "$DRYRUN" != "YES" ]; then
  # To get into action, the $TS_INPUTFILE needs to be newer then TS_IMAGE
  if [ "$TS_INPUTFILE" -gt "$TS_IMAGE" ]; then
    echo "# We need to build, let's do that ..."
    if [ -n "${BUILDDIR:+x}" ]; then
      #cp $INPUTFILE $BUILDDIR
      cd $BUILDDIR
    fi
    if [ -e "prepare.sh" ]; then
      echo "# Building docker-image ... prepare.sh" 
      ./prepare.sh 
      echo "# content---------"
      ls -l
      echo "#-----------------"
    fi
    echo "# Building docker-image ... docker build -t ${IMAGE}:${TS_INPUTFILE} ."
    docker build -t ${IMAGE}:${TS_INPUTFILE} .
    docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    docker push ${IMAGE}:${TS_INPUTFILE}
    echo "# done"
    
    CLONE_URL=$(git remote get-url origin | sed -e "s/.*@/git@/" -e "s/\//:/")
    echo "# cloning $CLONE_URL to update references ..."
    git clone $CLONE_URL checkout_directory
    cd checkout_directory && ls -la
    git checkout $CI_BUILD_REF_NAME

    for IMAGEREF in $IMAGEREFS ; do
      # pattern contains at least one trailing "/" in order to match better
      echo "# patching like sed -i $IMAGEREF -e \"s/${IMAGE##*/}:.*/${IMAGE##*/}:$TS_INPUTFILE/\" "
      sed -i $IMAGEREF -e "s/${IMAGE##*/}:.*/${IMAGE##*/}:$TS_INPUTFILE/"
      git add $IMAGEREF
    done

    git commit -m "robot: $INPUTFILE was newer, rebuilt image $IMAGE and adjusted $IMAGEREFS"
    git push
    echo "# done ... SUCCESS"
    echo "# returning non-zero exit-code to prevent continuation of pipeline!!"

    exit 2
  else
    echo "Nothing to do!!"
  fi
fi